/**
 * Created by xiemp on 2016/12/19.
 */
var dev_env = require('./dev')
var test_env = require('./test')


//根据不同Node_ENV,输出不同的配置对象，默认输出dev的配置对象
module.exports = {

    dev: dev_env,
    text:test_env
}[process.env.NODE_ENV || 'development']

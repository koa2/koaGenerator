/**
 * Created by xiemp on 2016/12/26.
 * router配置目录
 */
var router = require('koa-router')()
var user_controller = require('../../app/controllers/user_controller')

router.get('/getUser', user_controller.getUser);
router.post('/registerUser', user_controller.registerUser);

module.exports = router

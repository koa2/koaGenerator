/**
 * api接口内容配置
 * Created by xiemp on 2016/12/26.
 */
const ApiError = require('../error/ApiError');
const ApiErrorNames = require('../error/ApiErrorNames');

//获取用户
exports.getUser = async (ctx, next) => {
    //如果id != 1 抛出Api 异常
    if(ctx.query.id != 1){
        
       throw new ApiError(ApiErrorNames.USER_NOT_EXIST)
    }

    // ctx.body = {
    //     username: '阿巴斯',
    //     age: 30
    // }
}

//用户注册
exports.registerUser = async (ctx, next) => {
    console.log('registerUser', ctx.request.body)
}





